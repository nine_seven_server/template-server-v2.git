package com.zqh.www.service.wallet.param;

import java.math.BigDecimal;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.zqh.www.base.EditDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 钱包记录
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Data
@ApiModel(description = "钱包记录")
public class CreateOrEditWalletParam {

    /**
     * 钱包id
     * int(11) 11
     */
    @ApiModelProperty(value = "钱包id(新增不用传，修改要传)", required = false, position = 0)
    @NotNull(groups = {EditDomain.class}, message = "请传入用户主键")
    private Integer walletId;

    /**
     * 金额
     * decimal(15,3) 15,3
     */
    @ApiModelProperty(value = "金额", required = true, position = 1)
    @NotNull(message = "金额参数必传")
    private BigDecimal amount;

    /**
     * 用户id
     * int(11) 11
     */
    @ApiModelProperty(value = "用户id", required = true, position = 2)
    @NotNull(message = "用户id参数必传")
    private Integer userId;

}