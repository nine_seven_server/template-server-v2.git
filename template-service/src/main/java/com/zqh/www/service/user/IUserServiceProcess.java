package com.zqh.www.service.user;

import com.zqh.www.service.user.param.CreateOrEditUserParam;
import com.zqh.www.service.user.param.GetUserListParam;
import com.zqh.www.service.user.vo.UserInfoVo;
import com.zqh.www.base.TableRecordVo;
/**
 * <p>
 * 用户记录 服务类-流程
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
public interface IUserServiceProcess extends IUserService {

    /**
     * 创建
     * @param createOrEditUserParam
     */
    void userCreate(CreateOrEditUserParam createOrEditUserParam);

    /**
     * 编辑
     * @param createOrEditUserParam
     */
    void userEdit(CreateOrEditUserParam createOrEditUserParam);

    /**
     * 删除
     * @param userId
     */
    void userDelete(Integer userId);

    /**
     * 分页查询
     * @param getUserListParam
     * @return
     */
    TableRecordVo<UserInfoVo> getUserList(GetUserListParam getUserListParam);
}
