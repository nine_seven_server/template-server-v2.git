package com.zqh.www.config.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
public class NotPermissionException extends RuntimeException {

    final Exception ex;

    final String msg;

    public NotPermissionException(String msg) {
        super(msg);
        this.ex = null;
        this.msg = msg;
    }

    public NotPermissionException(Exception ex) {
        super(ex);
        this.ex = ex;
        this.msg = ex.getMessage();
    }
}
