package com.zqh.www.base;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * <p>
 * 表格记录通用返回对象
 * </p>
 *
 * @author z
 * @since 2020-02-23
 */
@Data
@Builder
//使用后添加一个构造函数，该构造函数含有所有已声明字段属性参数
@AllArgsConstructor
//使用后创建一个无参构造函数
@NoArgsConstructor
@ApiModel(value = "表格记录对象", description = "表格记录通用返回")
public class TableRecordVo<T> {

    private List<T> records;

    private Long total;

    private Long pages;

    private Object other;
}