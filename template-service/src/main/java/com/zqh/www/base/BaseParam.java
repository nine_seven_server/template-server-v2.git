package com.zqh.www.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "基础查询", description = "基础查询实体")
public class BaseParam implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "当前页数", example = "1", required = true)
    @NotNull(message = "当前页数不能为空")
    @Min(value = 1, message = "最小当前页数为1")
    private Integer pageNo;

    @ApiModelProperty(value = "每页显示数", example = "10", required = true)
    @NotNull(message = "每页显示数不能为空")
    @Min(value = 1, message = "最小每页显示数为1")
    @Max(value = 100, message = "最大每页显示数为100")
    private int pageSize;

    @ApiModelProperty(value = "开始时间")
    private String startDateTime;

    @ApiModelProperty(value = "结束时间")
    private String endDateTime;

    @ApiModelProperty(value = "排序")
    private String sort;
}
