package ${package.Controller};

import com.alibaba.fastjson.JSONObject;
<#if swagger2>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
</#if>
import com.zqh.www.base.EditDomain;
import com.zqh.www.base.TableRecordVo;
import com.zqh.www.config.R;
import ${package.Service}.param.CreateOrEdit${entity}Param;
import ${package.Service}.param.Get${entity}ListParam;
import ${package.Service}.${table.serviceName}Process;
import ${package.Service}.vo.${entity}InfoVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

import javax.validation.Valid;
/**
 * <p>
 * ${table.comment!?trim} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if swagger2>
@Api(tags = "${table.comment!?trim}")
</#if>
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
    class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
    <#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
    <#else>
public class ${table.controllerName} {
    </#if>

    @Autowired
    private ${table.serviceName}Process ${table.serviceName?substring(1)?uncap_first}Process;

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "${table.comment!?trim}(创建)", notes = "${table.comment!?trim}(创建)")
    @PostMapping("/${entity?uncap_first}Create")
    public R ${entity?uncap_first}Create(@Valid @RequestBody CreateOrEdit${entity}Param createOrEdit${entity}Param) {
        ${table.serviceName?substring(1)?uncap_first}Process.${entity?uncap_first}Create(createOrEdit${entity}Param);
        return R.ok(null, "新增成功");
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "${table.comment!?trim}(编辑)", notes = "${table.comment!?trim}(编辑)")
    @PostMapping("/${entity?uncap_first}Edit")
    public R ${entity?uncap_first}Edit(@Validated({EditDomain.class}) @RequestBody CreateOrEdit${entity}Param createOrEdit${entity}Param) {
        ${table.serviceName?substring(1)?uncap_first}Process.${entity?uncap_first}Edit(createOrEdit${entity}Param);
        return R.ok(null, "修改成功");
    }

    @ApiOperationSupport(order = 3)
    @DynamicParameters(name = "${entity?uncap_first}DeleteParam", properties = {
            @DynamicParameter(name = "<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName}</#if></#if></#list>", value = "1", required = true)
    })
    @ApiOperation(value = "${table.comment!?trim}(删除)", notes = "${table.comment!?trim}(删除)")
    @PostMapping("/${entity?uncap_first}Delete")
    public R ${entity?uncap_first}Delete(@Valid @RequestBody JSONObject ${entity?uncap_first}DeleteParam) {
        int <#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName}</#if></#if></#list> = ${entity?uncap_first}DeleteParam.getIntValue("<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName}</#if></#if></#list>");
        ${table.serviceName?substring(1)?uncap_first}Process.${entity?uncap_first}Delete(<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName}</#if></#if></#list>);
        return R.ok(null, "删除成功");
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "${table.comment!?trim}(列表)", notes = "${table.comment!?trim}(列表)")
    @PostMapping("/get${entity}List")
    public R<TableRecordVo<${entity}InfoVo>> get${entity}List(@Valid @RequestBody Get${entity}ListParam get${entity}ListParam) {
        TableRecordVo<${entity}InfoVo> tableRecordVo = ${table.serviceName?substring(1)?uncap_first}Process.get${entity}List(get${entity}ListParam);
        return R.ok(tableRecordVo, "获取成功");
    }
}
</#if>