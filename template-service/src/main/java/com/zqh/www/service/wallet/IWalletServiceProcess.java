package com.zqh.www.service.wallet;

import com.zqh.www.service.wallet.param.CreateOrEditWalletParam;
import com.zqh.www.service.wallet.param.GetWalletListParam;
import com.zqh.www.service.wallet.vo.WalletInfoVo;
import com.zqh.www.base.TableRecordVo;
/**
 * <p>
 * 钱包记录 服务类-流程
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
public interface IWalletServiceProcess extends IWalletService {

    /**
     * 创建
     * @param createOrEditWalletParam
     */
    void walletCreate(CreateOrEditWalletParam createOrEditWalletParam);

    /**
     * 编辑
     * @param createOrEditWalletParam
     */
    void walletEdit(CreateOrEditWalletParam createOrEditWalletParam);

    /**
     * 删除
     * @param walletId
     */
    void walletDelete(Integer walletId);

    /**
     * 分页查询
     * @param getWalletListParam
     * @return
     */
    TableRecordVo<WalletInfoVo> getWalletList(GetWalletListParam getWalletListParam);
}
