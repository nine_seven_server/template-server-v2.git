package com.zqh.www.mapper.wallet;

import com.zqh.www.entity.wallet.Wallet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 钱包记录 Mapper 接口
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Repository
public interface WalletMapper extends BaseMapper<Wallet> {

}
