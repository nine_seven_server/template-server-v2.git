package com.zqh.www.controller.user;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import com.zqh.www.base.EditDomain;
import com.zqh.www.base.TableRecordVo;
import com.zqh.www.config.R;
import com.zqh.www.service.user.param.CreateOrEditUserParam;
import com.zqh.www.service.user.param.GetUserListParam;
import com.zqh.www.service.user.IUserServiceProcess;
import com.zqh.www.service.user.vo.UserInfoVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
/**
 * <p>
 * 用户记录 前端控制器
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Api(tags = "用户记录")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserServiceProcess userServiceProcess;

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "用户记录(创建)", notes = "用户记录(创建)")
    @PostMapping("/userCreate")
    public R userCreate(@Valid @RequestBody CreateOrEditUserParam createOrEditUserParam) {
        userServiceProcess.userCreate(createOrEditUserParam);
        return R.ok(null, "新增成功");
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "用户记录(编辑)", notes = "用户记录(编辑)")
    @PostMapping("/userEdit")
    public R userEdit(@Validated({EditDomain.class}) @RequestBody CreateOrEditUserParam createOrEditUserParam) {
        userServiceProcess.userEdit(createOrEditUserParam);
        return R.ok(null, "修改成功");
    }

    @ApiOperationSupport(order = 3)
    @DynamicParameters(name = "userDeleteParam", properties = {
            @DynamicParameter(name = "userId", value = "1", required = true)
    })
    @ApiOperation(value = "用户记录(删除)", notes = "用户记录(删除)")
    @PostMapping("/userDelete")
    public R userDelete(@Valid @RequestBody JSONObject userDeleteParam) {
        int userId = userDeleteParam.getIntValue("userId");
        userServiceProcess.userDelete(userId);
        return R.ok(null, "删除成功");
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "用户记录(列表)", notes = "用户记录(列表)")
    @PostMapping("/getUserList")
    public R<TableRecordVo<UserInfoVo>> getUserList(@Valid @RequestBody GetUserListParam getUserListParam) {
        TableRecordVo<UserInfoVo> tableRecordVo = userServiceProcess.getUserList(getUserListParam);
        return R.ok(tableRecordVo, "获取成功");
    }
}
