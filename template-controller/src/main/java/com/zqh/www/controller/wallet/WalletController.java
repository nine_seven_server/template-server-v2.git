package com.zqh.www.controller.wallet;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import com.zqh.www.base.EditDomain;
import com.zqh.www.base.TableRecordVo;
import com.zqh.www.config.R;
import com.zqh.www.service.wallet.param.CreateOrEditWalletParam;
import com.zqh.www.service.wallet.param.GetWalletListParam;
import com.zqh.www.service.wallet.IWalletServiceProcess;
import com.zqh.www.service.wallet.vo.WalletInfoVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
/**
 * <p>
 * 钱包记录 前端控制器
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Api(tags = "钱包记录")
@RestController
@RequestMapping("/wallet")
public class WalletController {

    @Autowired
    private IWalletServiceProcess walletServiceProcess;

    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "钱包记录(创建)", notes = "钱包记录(创建)")
    @PostMapping("/walletCreate")
    public R walletCreate(@Valid @RequestBody CreateOrEditWalletParam createOrEditWalletParam) {
        walletServiceProcess.walletCreate(createOrEditWalletParam);
        return R.ok(null, "新增成功");
    }

    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "钱包记录(编辑)", notes = "钱包记录(编辑)")
    @PostMapping("/walletEdit")
    public R walletEdit(@Validated({EditDomain.class}) @RequestBody CreateOrEditWalletParam createOrEditWalletParam) {
        walletServiceProcess.walletEdit(createOrEditWalletParam);
        return R.ok(null, "修改成功");
    }

    @ApiOperationSupport(order = 3)
    @DynamicParameters(name = "walletDeleteParam", properties = {
            @DynamicParameter(name = "walletId", value = "1", required = true)
    })
    @ApiOperation(value = "钱包记录(删除)", notes = "钱包记录(删除)")
    @PostMapping("/walletDelete")
    public R walletDelete(@Valid @RequestBody JSONObject walletDeleteParam) {
        int walletId = walletDeleteParam.getIntValue("walletId");
        walletServiceProcess.walletDelete(walletId);
        return R.ok(null, "删除成功");
    }

    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "钱包记录(列表)", notes = "钱包记录(列表)")
    @PostMapping("/getWalletList")
    public R<TableRecordVo<WalletInfoVo>> getWalletList(@Valid @RequestBody GetWalletListParam getWalletListParam) {
        TableRecordVo<WalletInfoVo> tableRecordVo = walletServiceProcess.getWalletList(getWalletListParam);
        return R.ok(tableRecordVo, "获取成功");
    }
}
