package com.zqh.www.config.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
public class ServiceException extends RuntimeException {

    final Exception ex;

    final String msg;

    public ServiceException(String msg) {
        super(msg);
        this.ex = null;
        this.msg = msg;
    }

    public ServiceException(Exception ex) {
        super(ex);
        this.ex = ex;
        this.msg = ex.getMessage();
    }
}
