package com.zqh.www.service.user.param;

import java.math.BigDecimal;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.zqh.www.base.EditDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 用户记录
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Data
@ApiModel(description = "用户记录")
public class CreateOrEditUserParam {

    /**
     * 用户id
     * int(11) 11
     */
    @ApiModelProperty(value = "用户id(新增不用传，修改要传)", required = false, position = 0)
    @NotNull(groups = {EditDomain.class}, message = "请传入用户主键")
    private Integer userId;

    /**
     * 用户名称
     * char(8) 8
     */
    @ApiModelProperty(value = "用户名称", required = true, position = 1)
    @Length(max = 8, message = "用户名称参数过长")
    @NotBlank(message = "用户名称参数必传")
    private String userName;

    /**
     * 用户密码
     * char(32) 32
     */
    @ApiModelProperty(value = "用户密码", required = true, position = 2)
    @Length(max = 32, message = "用户密码参数过长")
    @NotBlank(message = "用户密码参数必传")
    private String userPassword;

}