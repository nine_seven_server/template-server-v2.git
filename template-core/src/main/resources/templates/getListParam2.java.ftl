package ${package.Service}.param;

import com.zqh.www.base.BaseParam;
<#if swagger2>
import io.swagger.annotations.ApiModel;
</#if>
<#if entityLombokModel>
import lombok.Data;
</#if>

/**
 * <p>
 * ${table.comment!?trim}
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Data
</#if>
<#if swagger2>
@ApiModel(description = "获取${table.comment!?trim}列表参数")
</#if>
public class Get${entity}ListParam extends BaseParam {

}