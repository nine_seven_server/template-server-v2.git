package com.zqh.www.service.user.param;

import com.zqh.www.base.BaseParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p>
 * 用户记录
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Data
@ApiModel(description = "获取用户记录列表参数")
public class GetUserListParam extends BaseParam {

}