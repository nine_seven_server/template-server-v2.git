package com.zqh.www.permissions;

/**
 * 权限常量类
 *
 * @author zengqinghua
 */
public class PermissionConstants {
    /**
     * 管理员-产品列表查询
     */
    public static final String ADMIN_PRODUCT_LIST = "admin_product_list";

}
