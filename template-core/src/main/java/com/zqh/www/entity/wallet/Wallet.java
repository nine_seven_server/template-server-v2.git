package com.zqh.www.entity.wallet;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zqh.www.base.BaseEntity;
import lombok.*;

/**
 * <p>
 * 钱包记录
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Data
//@EqualsAndHashCode(callSuper = true)
//@Builder
//使用后添加一个构造函数，该构造函数含有所有已声明字段属性参数
//@AllArgsConstructor
//使用后创建一个无参构造函数
//@NoArgsConstructor
@TableName("wallet")
public class Wallet extends BaseEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 钱包id
     * int(11) 11
     */
    @TableId(value = "wallet_id", type = IdType.AUTO)
    private Integer walletId;

    /**
     * 金额
     * decimal(15,3) 15,3
     */
    private BigDecimal amount;

    /**
     * 用户id
     * int(11) 11
     */
    private Integer userId;

}