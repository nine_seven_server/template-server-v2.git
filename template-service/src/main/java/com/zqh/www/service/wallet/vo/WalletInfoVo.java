package com.zqh.www.service.wallet.vo;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 钱包记录
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Data
@ApiModel(description = "钱包记录")
public class WalletInfoVo {

    /**
     * 钱包id
     */
    @ApiModelProperty(value = "钱包id")
    private Integer walletId;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Integer userId;

}