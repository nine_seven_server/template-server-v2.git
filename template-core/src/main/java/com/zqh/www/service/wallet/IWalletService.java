package com.zqh.www.service.wallet;

import com.zqh.www.entity.wallet.Wallet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 钱包记录 服务类
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
public interface IWalletService extends IService<Wallet> {

}
