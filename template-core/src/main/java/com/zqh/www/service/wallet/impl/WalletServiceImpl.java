package com.zqh.www.service.wallet.impl;

import com.zqh.www.entity.wallet.Wallet;
import com.zqh.www.mapper.wallet.WalletMapper;
import com.zqh.www.service.wallet.IWalletService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包记录 服务实现类
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Service
public class WalletServiceImpl extends ServiceImpl<WalletMapper, Wallet> implements IWalletService {

    @Autowired
    private WalletMapper walletMapper;

}
