package ${package.Service}.param;

import java.math.BigDecimal;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.zqh.www.base.EditDomain;
<#if swagger2>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
<#if entityLombokModel>
import lombok.Data;
</#if>

/**
 * <p>
 * ${table.comment!?trim}
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if entityLombokModel>
@Data
</#if>
<#if swagger2>
@ApiModel(description = "${table.comment!?trim}")
</#if>
public class CreateOrEdit${entity}Param {
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>

    <#if field.comment!?length gt 0>
        <#if swagger2>
            <#if "create_time" == field.name || "update_time" == field.name || "tb_status" == field.name>
            <#else>
    /**
     * ${field.comment}
     * ${field.type} ${field.type?substring(field.type?index_of("(")+1,field.type?length - 1)}
     */
    <#if field.keyFlag>
    @ApiModelProperty(value = "${field.comment}(新增不用传，修改要传)", required = false, position = ${field_index})
    @NotNull(groups = {EditDomain.class}, message = "请传入用户主键")
    <#else>
    @ApiModelProperty(value = "${field.comment}", required = true, position = ${field_index})
        <#if field.propertyType == 'String'>
            <#if field.type != 'text'>
    @Length(max = ${field.type?substring(field.type?index_of("(")+1,field.type?length - 1)}, message = "${field.comment}参数过长")
            </#if>
    @NotBlank(message = "${field.comment}参数必传")
        <#else>
    @NotNull(message = "${field.comment}参数必传")
        </#if>
    </#if>
            </#if>
        <#else>
            <#if "create_time" == field.name || "update_time" == field.name || "tb_status" == field.name>
            <#else>
    /**
     * ${field.comment}
     * ${field.type} ${field.type?substring(field.type?index_of("(")+1,field.type?length - 1)}
     */
            </#if>
        </#if>
    </#if>
    <#if field.keyFlag>
    <#-- 普通字段 -->
    <#elseif field.fill??>
    <#-- -----   存在字段填充设置   ----->
        <#if field.convert>
    @TableField(value = "${field.name}", fill = FieldFill.${field.fill})
        <#else>
    @TableField(fill = FieldFill.${field.fill})
        </#if>
    <#elseif field.convert>
    @TableField("${field.name}")
    </#if>
<#-- 乐观锁注解 -->
    <#if (versionFieldName!"") == field.name>
    @Version
    </#if>
<#-- 逻辑删除注解 -->
    <#if (logicDeleteFieldName!"") == field.name>
    @TableLogic
    </#if>
    <#if "create_time" == field.name || "update_time" == field.name || "tb_status" == field.name>
    <#else>
    private ${field.propertyType} ${field.propertyName};
    </#if>
</#list>
<#------------  END 字段循环遍历  ---------->

<#if !entityLombokModel>
    <#list table.fields as field>
        <#if field.propertyType == "boolean">
            <#assign getprefix="is"/>
        <#else>
            <#assign getprefix="get"/>
        </#if>
        public ${field.propertyType} ${getprefix}${field.capitalName}() {
        return ${field.propertyName};
        }
        <#if entityBuilderModel>
            public ${entity} set${field.capitalName}(${field.propertyType} ${field.propertyName}) {
        <#else>
            public void set${field.capitalName}(${field.propertyType} ${field.propertyName}) {
        </#if>
        this.${field.propertyName} = ${field.propertyName};
        <#if entityBuilderModel>
            return this;
        </#if>
        }
    </#list>
</#if>
<#if activeRecord>
    @Override
    protected Serializable pkVal() {
    <#if keyPropertyName??>
        return this.${keyPropertyName};
    <#else>
        return null;
    </#if>
    }
</#if>
<#if !entityLombokModel>
    @Override
    public String toString() {
    return "${entity}{" +
    <#list table.fields as field>
        <#if field_index==0>
            "${field.propertyName}=" + ${field.propertyName} +
        <#else>
            ", ${field.propertyName}=" + ${field.propertyName} +
        </#if>
    </#list>
    "}";
    }
</#if>
}