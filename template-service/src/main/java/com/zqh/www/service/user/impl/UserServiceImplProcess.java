package com.zqh.www.service.user.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zqh.www.base.TableRecordVo;
import com.zqh.www.config.exceptions.ServiceException;
import com.zqh.www.entity.user.User;
import com.zqh.www.service.user.param.CreateOrEditUserParam;
import com.zqh.www.service.user.param.GetUserListParam;
import com.zqh.www.service.user.IUserService;
import com.zqh.www.service.user.IUserServiceProcess;
import com.zqh.www.service.user.vo.UserInfoVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户记录 服务实现类-流程
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Service
public class UserServiceImplProcess extends UserServiceImpl implements IUserServiceProcess {

    /**
     * 创建
     * @param createOrEditUserParam
     */
    @Override
    public void userCreate(CreateOrEditUserParam createOrEditUserParam) {
        User user = new User();
        BeanUtils.copyProperties(createOrEditUserParam, user);
        boolean save = this.save(user);
        if (!save) {
            throw new ServiceException("保存失败");
        }
    }

    /**
     * 编辑
     * @param createOrEditUserParam
     */
    @Override
    public void userEdit(CreateOrEditUserParam createOrEditUserParam) {
        if (null == createOrEditUserParam.getUserId() || 0 == createOrEditUserParam.getUserId()) {
            throw new ServiceException("参数错误");
        }
        User user = this.getById(createOrEditUserParam.getUserId());
        if (null == user) {
            throw new ServiceException("记录不存在");
        }
        BeanUtils.copyProperties(createOrEditUserParam, user);
        boolean update = this.updateById(user);
        if (!update) {
            throw new ServiceException("编辑失败");
        }
    }

    /**
     * 删除
     * @param userId
     */
    @Override
    public void userDelete(Integer userId) {
        User user = this.getById(userId);
        if (null == user) {
            throw new ServiceException("记录不存在");
        }
        boolean remove = this.updateById(user);
        if (!remove) {
            throw new ServiceException("删除失败");
        }
    }

    /**
     * 分页查询
     * @param getUserListParam
     * @return
     */
    @Override
    public TableRecordVo<UserInfoVo> getUserList(GetUserListParam getUserListParam) {
        LambdaQueryWrapper<User> queryWrapper = new QueryWrapper<User>().lambda();
        queryWrapper.eq(User::getTbStatus, "正常")
                .ge(StringUtils.isNotBlank(getUserListParam.getStartDateTime()), User::getCreateTime, getUserListParam.getStartDateTime())
                .le(StringUtils.isNotBlank(getUserListParam.getEndDateTime()), User::getCreateTime, getUserListParam.getEndDateTime())
                .orderBy(StringUtils.isNotBlank(getUserListParam.getSort()), "asc".equals(getUserListParam.getSort()), User::getUserId);

        IPage<User> page = this.page(new Page<>(getUserListParam.getPageNo(), getUserListParam.getPageSize()), queryWrapper);

        List<UserInfoVo> userInfoVoList = page.getRecords().stream().map(user -> {
            UserInfoVo userInfoVo = new UserInfoVo();
            BeanUtils.copyProperties(user, userInfoVo);
            return userInfoVo;
        }).collect(Collectors.toList());

        TableRecordVo tableRecordVo = new TableRecordVo<>();
        tableRecordVo.setRecords(userInfoVoList);
        tableRecordVo.setTotal(page.getTotal());
        tableRecordVo.setPages(page.getPages());
        return tableRecordVo;
    }

}