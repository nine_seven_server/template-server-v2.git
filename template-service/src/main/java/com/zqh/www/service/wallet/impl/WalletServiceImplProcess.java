package com.zqh.www.service.wallet.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zqh.www.base.TableRecordVo;
import com.zqh.www.config.exceptions.ServiceException;
import com.zqh.www.entity.wallet.Wallet;
import com.zqh.www.service.wallet.param.CreateOrEditWalletParam;
import com.zqh.www.service.wallet.param.GetWalletListParam;
import com.zqh.www.service.wallet.IWalletService;
import com.zqh.www.service.wallet.IWalletServiceProcess;
import com.zqh.www.service.wallet.vo.WalletInfoVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 钱包记录 服务实现类-流程
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Service
public class WalletServiceImplProcess extends WalletServiceImpl implements IWalletServiceProcess {

    /**
     * 创建
     * @param createOrEditWalletParam
     */
    @Override
    public void walletCreate(CreateOrEditWalletParam createOrEditWalletParam) {
        Wallet wallet = new Wallet();
        BeanUtils.copyProperties(createOrEditWalletParam, wallet);
        boolean save = this.save(wallet);
        if (!save) {
            throw new ServiceException("保存失败");
        }
    }

    /**
     * 编辑
     * @param createOrEditWalletParam
     */
    @Override
    public void walletEdit(CreateOrEditWalletParam createOrEditWalletParam) {
        if (null == createOrEditWalletParam.getWalletId() || 0 == createOrEditWalletParam.getWalletId()) {
            throw new ServiceException("参数错误");
        }
        Wallet wallet = this.getById(createOrEditWalletParam.getWalletId());
        if (null == wallet) {
            throw new ServiceException("记录不存在");
        }
        BeanUtils.copyProperties(createOrEditWalletParam, wallet);
        boolean update = this.updateById(wallet);
        if (!update) {
            throw new ServiceException("编辑失败");
        }
    }

    /**
     * 删除
     * @param walletId
     */
    @Override
    public void walletDelete(Integer walletId) {
        Wallet wallet = this.getById(walletId);
        if (null == wallet) {
            throw new ServiceException("记录不存在");
        }
        boolean remove = this.updateById(wallet);
        if (!remove) {
            throw new ServiceException("删除失败");
        }
    }

    /**
     * 分页查询
     * @param getWalletListParam
     * @return
     */
    @Override
    public TableRecordVo<WalletInfoVo> getWalletList(GetWalletListParam getWalletListParam) {
        LambdaQueryWrapper<Wallet> queryWrapper = new QueryWrapper<Wallet>().lambda();
        queryWrapper.eq(Wallet::getTbStatus, "正常")
                .ge(StringUtils.isNotBlank(getWalletListParam.getStartDateTime()), Wallet::getCreateTime, getWalletListParam.getStartDateTime())
                .le(StringUtils.isNotBlank(getWalletListParam.getEndDateTime()), Wallet::getCreateTime, getWalletListParam.getEndDateTime())
                .orderBy(StringUtils.isNotBlank(getWalletListParam.getSort()), "asc".equals(getWalletListParam.getSort()), Wallet::getWalletId);

        IPage<Wallet> page = this.page(new Page<>(getWalletListParam.getPageNo(), getWalletListParam.getPageSize()), queryWrapper);

        List<WalletInfoVo> walletInfoVoList = page.getRecords().stream().map(wallet -> {
            WalletInfoVo walletInfoVo = new WalletInfoVo();
            BeanUtils.copyProperties(wallet, walletInfoVo);
            return walletInfoVo;
        }).collect(Collectors.toList());

        TableRecordVo tableRecordVo = new TableRecordVo<>();
        tableRecordVo.setRecords(walletInfoVoList);
        tableRecordVo.setTotal(page.getTotal());
        tableRecordVo.setPages(page.getPages());
        return tableRecordVo;
    }

}