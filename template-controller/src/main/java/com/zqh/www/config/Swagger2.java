package com.zqh.www.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc
public class Swagger2 {
    @Value("${swaggersetting.enable}")
    private Boolean enable;

    @Bean
    public Docket createBaseApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(this.enable)
                .groupName("接口文档")
                .apiInfo(baseApiInfo()).select()
                .apis(RequestHandlerSelectors.basePackage("com.zqh.www"))
                .paths(PathSelectors.regex(".*/*/.*"))
                .build();
    }

    private ApiInfo baseApiInfo() {
        return new ApiInfoBuilder().title("接口文档").description("接口文档").termsOfServiceUrl("").version("1.0").build();
    }
}
