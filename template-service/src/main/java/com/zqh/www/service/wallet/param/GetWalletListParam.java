package com.zqh.www.service.wallet.param;

import com.zqh.www.base.BaseParam;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p>
 * 钱包记录
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Data
@ApiModel(description = "获取钱包记录列表参数")
public class GetWalletListParam extends BaseParam {

}