package com.zqh.www.service.user;

import com.zqh.www.entity.user.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户记录 服务类
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
public interface IUserService extends IService<User> {

}
