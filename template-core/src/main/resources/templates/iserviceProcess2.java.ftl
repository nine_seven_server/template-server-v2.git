package ${package.Service};

import ${package.Service}.param.CreateOrEdit${entity}Param;
import ${package.Service}.param.Get${entity}ListParam;
import ${package.Service}.vo.${entity}InfoVo;
import com.zqh.www.base.TableRecordVo;
/**
 * <p>
 * ${table.comment!?trim} 服务类-流程
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
public interface ${table.serviceName}Process extends ${table.serviceName} {

    /**
     * 创建
     * @param createOrEdit${entity}Param
     */
    void ${entity?uncap_first}Create(CreateOrEdit${entity}Param createOrEdit${entity}Param);

    /**
     * 编辑
     * @param createOrEdit${entity}Param
     */
    void ${entity?uncap_first}Edit(CreateOrEdit${entity}Param createOrEdit${entity}Param);

    /**
     * 删除
     * @param <#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName}</#if></#if></#list>
     */
    void ${entity?uncap_first}Delete(Integer <#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName}</#if></#if></#list>);

    /**
     * 分页查询
     * @param get${entity}ListParam
     * @return
     */
    TableRecordVo<${entity}InfoVo> get${entity}List(Get${entity}ListParam get${entity}ListParam);
}
