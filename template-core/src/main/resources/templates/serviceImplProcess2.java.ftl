package ${package.ServiceImpl};

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zqh.www.base.TableRecordVo;
import com.zqh.www.config.exceptions.ServiceException;
import ${package.Entity}.${entity};
import ${package.Service}.param.CreateOrEdit${entity}Param;
import ${package.Service}.param.Get${entity}ListParam;
import ${package.Service}.${table.serviceName};
import ${package.Service}.${table.serviceName}Process;
import ${package.Service}.vo.${entity}InfoVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * ${table.comment!?trim} 服务实现类-流程
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
public class ${table.serviceImplName}Process extends ${table.serviceImplName} implements ${table.serviceName}Process {

    /**
     * 创建
     * @param createOrEdit${entity}Param
     */
    @Override
    public void ${entity?uncap_first}Create(CreateOrEdit${entity}Param createOrEdit${entity}Param) {
        ${entity} ${entity?uncap_first} = new ${entity}();
        BeanUtils.copyProperties(createOrEdit${entity}Param, ${entity?uncap_first});
        boolean save = this.save(${entity?uncap_first});
        if (!save) {
            throw new ServiceException("保存失败");
        }
    }

    /**
     * 编辑
     * @param createOrEdit${entity}Param
     */
    @Override
    public void ${entity?uncap_first}Edit(CreateOrEdit${entity}Param createOrEdit${entity}Param) {
        if (null == createOrEdit${entity}Param.get<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName?cap_first}</#if></#if></#list>() || 0 == createOrEdit${entity}Param.get<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName?cap_first}</#if></#if></#list>()) {
            throw new ServiceException("参数错误");
        }
        ${entity} ${entity?uncap_first} = this.getById(createOrEdit${entity}Param.get<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName?cap_first}</#if></#if></#list>());
        if (null == ${entity?uncap_first}) {
            throw new ServiceException("记录不存在");
        }
        BeanUtils.copyProperties(createOrEdit${entity}Param, ${entity?uncap_first});
        boolean update = this.updateById(${entity?uncap_first});
        if (!update) {
            throw new ServiceException("编辑失败");
        }
    }

    /**
     * 删除
     * @param <#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName}</#if></#if></#list>
     */
    @Override
    public void ${entity?uncap_first}Delete(Integer <#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName}</#if></#if></#list>) {
        ${entity} ${entity?uncap_first} = this.getById(<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName}</#if></#if></#list>);
        if (null == ${entity?uncap_first}) {
            throw new ServiceException("记录不存在");
        }
        boolean remove = this.updateById(${entity?uncap_first});
        if (!remove) {
            throw new ServiceException("删除失败");
        }
    }

    /**
     * 分页查询
     * @param get${entity}ListParam
     * @return
     */
    @Override
    public TableRecordVo<${entity}InfoVo> get${entity}List(Get${entity}ListParam get${entity}ListParam) {
        LambdaQueryWrapper<${entity}> queryWrapper = new QueryWrapper<${entity}>().lambda();
        queryWrapper.eq(${entity}::getTbStatus, "正常")
                .ge(StringUtils.isNotBlank(get${entity}ListParam.getStartDateTime()), ${entity}::getCreateTime, get${entity}ListParam.getStartDateTime())
                .le(StringUtils.isNotBlank(get${entity}ListParam.getEndDateTime()), ${entity}::getCreateTime, get${entity}ListParam.getEndDateTime())
                .orderBy(StringUtils.isNotBlank(get${entity}ListParam.getSort()), "asc".equals(get${entity}ListParam.getSort()), ${entity}::get<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyName?cap_first}</#if></#if></#list>);

        IPage<${entity}> page = this.page(new Page<>(get${entity}ListParam.getPageNo(), get${entity}ListParam.getPageSize()), queryWrapper);

        List<${entity}InfoVo> ${entity?uncap_first}InfoVoList = page.getRecords().stream().map(${entity?uncap_first} -> {
            ${entity}InfoVo ${entity?uncap_first}InfoVo = new ${entity}InfoVo();
            BeanUtils.copyProperties(${entity?uncap_first}, ${entity?uncap_first}InfoVo);
            return ${entity?uncap_first}InfoVo;
        }).collect(Collectors.toList());

        TableRecordVo tableRecordVo = new TableRecordVo<>();
        tableRecordVo.setRecords(${entity?uncap_first}InfoVoList);
        tableRecordVo.setTotal(page.getTotal());
        tableRecordVo.setPages(page.getPages());
        return tableRecordVo;
    }

}