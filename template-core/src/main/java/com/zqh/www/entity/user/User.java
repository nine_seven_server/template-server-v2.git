package com.zqh.www.entity.user;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zqh.www.base.BaseEntity;
import lombok.*;

/**
 * <p>
 * 用户记录
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Data
//@EqualsAndHashCode(callSuper = true)
//@Builder
//使用后添加一个构造函数，该构造函数含有所有已声明字段属性参数
//@AllArgsConstructor
//使用后创建一个无参构造函数
//@NoArgsConstructor
@TableName("user")
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 用户id
     * int(11) 11
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 用户名称
     * char(8) 8
     */
    private String userName;

    /**
     * 用户密码
     * char(32) 32
     */
    private String userPassword;

}