package com.zqh.www.permissions;

import java.lang.annotation.*;

/**
 * @author zengqinghua
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface RequiredPermission {
    String value();
}
