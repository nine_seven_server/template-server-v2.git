package com.zqh.www.service.user.impl;

import com.zqh.www.entity.user.User;
import com.zqh.www.mapper.user.UserMapper;
import com.zqh.www.service.user.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户记录 服务实现类
 * </p>
 *
 * @author z
 * @since 2020-12-02
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;

}
